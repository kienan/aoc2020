const std = @import("std");

pub fn main() anyerror!void {
    var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    defer arena.deinit();
    var gpa = &arena.allocator;

    var map = std.hash_map.AutoHashMap(u64, u64).init(gpa);
    defer map.deinit();

    var f = try std.fs.cwd().openFile("input", .{});
    var contents = try f.readToEndAlloc(gpa, std.math.maxInt(u32));

    // Part one
    var and_mask : u64 = std.math.maxInt(u64);
    var or_mask : u64 = 0;

    var it = std.mem.tokenize(contents, "\n");
    while (it.next()) |line| {
        std.log.debug("{}", .{line});
        if (std.mem.eql(u8, line[0..4], "mask")) {
            // Set mask
            var mask_iter = std.mem.tokenize(line, "=");
            _ = mask_iter.next(); // skip the first one
            if (mask_iter.next()) |v| {
                set_masks(std.mem.trim(u8, v, " "), &and_mask, &or_mask);
            }
        }
        else {
            var memory_location: u64 = 0;
            var memory_value: u64 = 0;
            var mem_iter = std.mem.tokenize(line, "=[");
            std.log.warn("{}", .{mem_iter.next()}); // skip 'mem['
            if (mem_iter.next()) |location_line| {
                std.log.debug("parsing '{}'", .{location_line});
                memory_location = try std.fmt.parseUnsigned(u64, std.mem.trim(u8, location_line, " ]"), 10);
            }
            if (mem_iter.next()) |value_line| {
                memory_value = try std.fmt.parseUnsigned(u64, std.mem.trim(u8, value_line, " "), 10);
            }
            try write_memory(&map, memory_location, memory_value, and_mask, or_mask);
        }
    }

    var map_it = map.iterator();
    var sum : u64 = 0;
    while (map_it.next()) |entry| {
        sum += entry.value;
    }
    std.log.info("Part 1: Sum of non-zero memory locations: {}", .{sum});

    // Part 2
    map.deinit();
    map = std.hash_map.AutoHashMap(u64, u64).init(gpa);
    gpa.free(contents);
    try f.seekTo(0);
    contents = try f.readToEndAlloc(gpa, std.math.maxInt(u32));
    it = std.mem.tokenize(contents, "\n");
    var address_mask : [36]u8 = undefined;
    while (it.next()) |line| {
        std.log.debug("{}", .{line});
        if (std.mem.eql(u8, line[0..4], "mask")) {
            // Set mask
            var mask_iter = std.mem.tokenize(line, "=");
            _ = mask_iter.next(); // skip the first one
            if (mask_iter.next()) |v| {
                std.mem.copy(u8, &address_mask, std.mem.trim(u8, v, " "));
            }
        }
        else {
            var memory_location: u64 = 0;
            var memory_value: u64 = 0;
            var mem_iter = std.mem.tokenize(line, "=[");
            _ = mem_iter.next(); // skip 'mem['
            if (mem_iter.next()) |location_line| {
                std.log.debug("parsing '{}'", .{location_line});
                memory_location = try std.fmt.parseUnsigned(u64, std.mem.trim(u8, location_line, " ]"), 10);
            }
            if (mem_iter.next()) |value_line| {
                memory_value = try std.fmt.parseUnsigned(u64, std.mem.trim(u8, value_line, " "), 10);
            }
            try write_memory_address_masked(&map, memory_location, memory_value, address_mask[0..]);
        }
    }
    map_it = map.iterator();
    sum = 0;
    while (map_it.next()) |entry| {
        sum += entry.value;
    }
    std.log.info("Part 2: Sum of non-zero memory locations: {}", .{sum});
}

fn write_memory_address_masked(map: *std.hash_map.AutoHashMap(u64, u64), location: u64, value: u64,
                               mask: []const u8) !void {
    // Fuse the value into the character mask
    var i : u6 = 0;
    var new_mask : [36]u8 = undefined;
    std.mem.copy(u8, &new_mask, mask);
    while (i < 36) : (i += 1) {
        if (new_mask[i] == 'X') {
            continue;
        }
        else if (new_mask[i] == '1') {
            continue;
        }
        else if (new_mask[i] == '0') {
            // mask to isolate bit at position 35 - i
            // kth() counts from LSB to MSB, we are going in string order from
            // MSB to LSB
            new_mask[i] = switch(kth_bit(location, 35-i)) {
                0 => '0',
                1 => '1',
                else => {
                    unreachable;
                },
            };
            //std.log.warn("Index {}: {c} -> {c}", .{i, new_mask[i], mask[i]});
        }
    }
    std.log.warn("\nValue:    {b:36} ({})\nPre-fuse: {}\nPst-fuse: {}", .{location, location, mask, new_mask});
    var addresses = try generate_address_list(map.allocator, new_mask[0..]);
    defer addresses.deinit();

    for (addresses.items) |address| {
        std.log.warn("Wrote {} to address {}", .{value, address});
        try map.put(address, value);
    }
}

// Caller is responsible for freeing this
// This function takes the "fused mask", where the only thing left to resolve
// if the 'X' bits.
fn generate_address_list(allocator: *std.mem.Allocator, mask: []const u8) !std.ArrayList(u64) {
    var masks = std.ArrayList(u64).init(allocator);
    var i : u64 = 0;
    // Store the offsets of the locations of the X character
    var x_locations = std.ArrayList(usize).init(allocator);
    defer x_locations.deinit();
    for (mask) |c, k| {
        if (c == 'X') {
            try x_locations.append(k);
        }
    }
    var n_variants = std.math.pow(u64, 2, x_locations.items.len);
    //std.log.warn("{} Xs in mask '{}', {} variants", .{x_locations.items.len, mask, n_variants});
    while (i < n_variants) : (i += 1) {
        var m = try allocator.alloc(u8, mask.len);
        std.mem.copy(u8, m, mask);
        var bit_index : u6 = 0;
        while (bit_index < x_locations.items.len) : (bit_index += 1) {
            // At 0, all Xs become zero,
            // At 1, last X becomes one, otherwise Xs are zero
            // ...
            m[x_locations.items[bit_index]] = switch(kth_bit(i, bit_index)) {
                0 => '0',
                1 => '1',
                else => {
                    unreachable;
                },
            };
        }
        var value = try std.fmt.parseUnsigned(u64, m, 2);
        //std.log.warn("\nFrom: {:64}\nGot:  {b:64}", .{m, value});
        try masks.append(value);
        allocator.free(m);
    }
    return masks;
}

// @see https://www.geeksforgeeks.org/find-value-k-th-bit-binary-representation/
fn kth_bit(value: u64, k: u6) u64 {
    //std.debug.warn("\nKey: {}, V: {}\nOrig: {b:64}\n", .{k, value, value});
    var one: u64 = 1;
    var shifted: u64 = 0;
    var overflowed = @shlWithOverflow(u64, one, k, &shifted);
    if (overflowed) {
        std.log.warn("OVERFLOWED", .{});
    }
    //std.debug.warn("Shft: {b:64}\n", .{shifted});
    //std.debug.warn("Andd: {b:64}\n", .{value & shifted});
    //std.debug.warn("Rslt: {b:64}\n", .{(value & shifted) >> k});
    return (value & shifted) >> k;
}

fn write_memory(map: *std.hash_map.AutoHashMap(u64, u64), location: u64, value: u64, and_mask: u64, or_mask: u64) !void {
    // Mask
    var new_value = value;
    new_value &= and_mask;
    new_value |= or_mask;
    try map.put(location, new_value);
    std.log.debug("Wrote '{}' to '{}' (pre-masked: '{}')",
                 .{new_value, location, value});
    //std.log.warn("\npre_mask: {b:64}\nand_mask: {b:64}\nor_mask  :{b:64}\npst_mask: {b:64}",
    //            .{value, and_mask, or_mask, new_value});
}

//const maskWidth : u64 = 36;
fn set_masks(line: []const u8, and_mask: *u64, or_mask: *u64) void {
    and_mask.* = std.math.maxInt(u64);
    or_mask.* = 0;

    var one: u64 = 1;
    for (line) |c, k| {
        var offset = @intCast(u6, line.len - k - 1);
        switch(c) {
            '1' => {
                // Only need to tweak the or_mask
                or_mask.* |= (one << offset);
            },
            '0' => {
                and_mask.* &= ~(one << offset);
            },
            'X' => {
                continue;
            },
            else => unreachable,
        }
    }
    std.log.debug("and mask:{b:64}", .{and_mask});
    std.log.debug("or  mask:{b:64}", .{or_mask});
}

test "mask_setting" {
    var and_mask : u64 = 0;
    var or_mask : u64 = 0;
    var l = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X";
    set_masks(l, &and_mask, &or_mask);
}

test "memory_writing" {
    var and_mask : u64 = 0;
    var or_mask : u64 = 0;
    var l = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X";
    set_masks(l, &and_mask, &or_mask);

    var map = std.hash_map.AutoHashMap(u64, u64).init(std.testing.allocator);
    defer map.deinit();

    try write_memory(&map, 8, 11, and_mask, or_mask);
    try write_memory(&map, 7, 101, and_mask, or_mask);
    try write_memory(&map, 8, 0, and_mask, or_mask);

    if (map.get(8)) |v| {
        std.testing.expectEqual(v, 64);
    }
    else {
        unreachable;
    }

    if (map.get(7)) |v| {
        std.testing.expectEqual(v, 101);
    }
    else {
        unreachable;
    }
}

test "kth_bit" {
    var value : u64 = 13;
    // 0000 1101
    var expected = [_]u64 {
        // goes from LSB to MSB
        1, 0, 1, 1,
        0, 0, 0, 0,
    };
    for (expected) |v, k| {
        var key : u6 = @intCast(u6, k);
        std.testing.expectEqual(kth_bit(value, key), expected[k]);
    }
}

test "generate_address_list" {
    var mask = "000000000000000000000000000000X1101X";
    var addresses = try generate_address_list(std.testing.allocator, mask[0..]);
    defer addresses.deinit();

    //std.log.warn("\n26:   {b:64}\n27:   {b:64}\n58:   {b:64}\n59:   {b:64}\n", .{26, 27, 58, 59});
    std.testing.expectEqual(addresses.items.len, 4);
    std.testing.expectEqual(addresses.items[0], 26);
    std.testing.expectEqual(addresses.items[1], 58);
    std.testing.expectEqual(addresses.items[2], 27);
    std.testing.expectEqual(addresses.items[3], 59);
}

test "part2" {
    var map = std.hash_map.AutoHashMap(u64, u64).init(std.testing.allocator);
    defer map.deinit();
    try write_memory_address_masked(&map, 42, 100, "000000000000000000000000000000X1001X");
    try write_memory_address_masked(&map, 26, 1, "00000000000000000000000000000000X0XX");
    var map_it = map.iterator();
    var sum : u64 = 0;
    while (map_it.next()) |entry| {
        sum += entry.value;
    }
    std.testing.expectEqual(sum, 208);
}
